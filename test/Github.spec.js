import _ from "lodash";
const axios = require("axios");

describe("Github Endpoints", () => {
  describe("User", () => {
    test("GET /user/wellmsan", async () => {
      const response = await axios("https://api.github.com/users/wellmsan");

      expect(response.data).toBeTruthy();
      //   expect(response.data.length).toBeTruthy();
    });

    test("Name is WELBER", async () => {
      const response = await axios("https://api.github.com/users/wellmsan");

      expect(response.data.name).toBe("WELBER");
    });
  });

  describe("Repos", () => {
    test("GET /user/wellmsan/repos", async () => {
      
      const response = await axios("https://api.github.com/users/wellmsan/repos");

      expect(response.data).toBeTruthy();
      expect(response.data.length).toBeTruthy();
      expect(response.data.length).toBe(8);
    });
  });

  describe("Starred", () => {
    test("GET /user/wellmsan/starred", async () => {
      
      const response = await axios("https://api.github.com/users/wellmsan/starred");

      expect(response.data).toBeTruthy();
      expect(response.data.length).toBeTruthy();
      expect(response.data.length).toBe(2);
    });
  });

});
