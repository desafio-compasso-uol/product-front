# product-front
## Desafio Compasso UOL
## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate

Browse URL: http://localhost:3000
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

# Objetivo

Nosso objetivo com este passo do processo de recrutamento é conhecer melhor as suas habilidades técnicas.
Com isso, selecionaremos quais desafios passaremos para você e quais precisaremos prepará-lo para melhor para enfrentar.

# Requisitos da Entrega
Para utilizar os Endpoints abaixo, você precisará estar autenticado, para isso você irá utilizar a autenticação do GITHUB:

- Guia Autenticação: https://docs.github.com/pt/developers/apps/building-oauth-apps
Gostaríamos nos entregasse uma aplicação utilizando a api do GITHUB https://developer.github.com/v3/ consumindo os seguintes endpoints:

- Endpoint user: https://api.github.com/users/NOME_USUARIO
- Endpoint repos: https://api.github.com/users/NOME_USUARIO/repos
- Endpoint starred: https://api.github.com/users/NOME_USUARIO/starred{/owner}{/repo}

A aplicação deverá constituir três componentes principais:

- O campo de busca.
- Visualização de resultados.
- Dois botões para executar um determinado resultado.

Ao clicar nos botões de repos e starred, deverá mostrar uma lista simples de cada endpoint apresentado anteriormente.

Dado um determinado usuário, deverá ser possível navegar diretamente até a página de detalhe do usuário sem que seja necessário efetuar uma nova busca. Ex: http://localhost:3000/NOME_USUARIO

- Gostariamos de pesquisar por usuario.
- Gostariamos de ao clicar no botão de repos, listar repositorios do usuario pesquisado.
- Gostariamos de ao clicar no botão de starred, listar os repositorios mais visitados por aquele usuario.

Você poderá usar o framework css Bootstrap ou Materialize para construção dos componentes UI (Se preferir, os componentes poderão ser criados do zero, utilizando as boas práticas).

Você poderá usar os frameworks js para desenvolvimento da sua aplicação ou utilizar o Vanilajs e jQuery.

Você poderá utilizar Jasmine, Mocha ou RhinoUnit para testar os request feitos.

![](/static/gitlab/01.png)
![](/static/gitlab/02.png)
![](/static/gitlab/03.png)
![](/static/gitlab/04.png)
![](/static/gitlab/08.png)
![](/static/gitlab/05.png)
![](/static/gitlab/06.png)
