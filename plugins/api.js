export default function ({ $axios, redirect, $auth, store }, inject) {
    // Create a custom axios instance
    const api = $axios.create({
        headers: {
            common: {
                Accept: 'application/json; charset=utf-8'
            }
        }
    })

    $axios.onRequest(config => {
        console.log('Making request to ' + config.url)
    })

    $axios.onError(error => {
        const code = parseInt(error.response && error.response.status)
        if (code === 500) {
            redirect('/sorry')
        }
    })

    // Inject to context as $api
    inject('api', api)
}