
// actions
export const actions = {
    reset({ commit }) {
        commit('userAuth/RESET');
        commit('repo/RESET');
        commit('starred/RESET');
        commit('user/RESET');
    },

}