export const state = () => ({
    data: [],
})

export const getInitialSate = () => {
    return {
        data: [],
    }
}

// getters
export const getters = {
    data: (state) => { return state.data },
}

// actions
export const actions = {
    reset({ commit }) {
        commit('RESET');
    },

    async loadAll({ commit }, endPoint) {
        const res = await this.$axios.get(endPoint)
        commit('SET_DATA', res.data)
    },
}

// mutations
export const mutations = {
    RESET(state) {
        const newState = getInitialSate();
        Object.keys(newState).forEach(key => {
            state[key] = newState[key]
        });
    },

    SET_DATA(state, data) {
        state.data = data
    },
    
}

