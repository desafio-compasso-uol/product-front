export const state = () => ({
    object: null
})

export const getInitialSate = () => {
    return {
        object: null
    }
}

// getters
export const getters = {
    object: (state) => { return state.object },
}

// actions
export const actions = {
    reset({ commit }) {
        commit('RESET');
    },

    async get({ commit }, endPoint) {
        const res = await this.$axios.get(endPoint)
        commit('SET_OBJECT', res.data)
    },
}

// mutations
export const mutations = {
    RESET(state) {
        const newState = getInitialSate();
        Object.keys(newState).forEach(key => {
            state[key] = newState[key]
        });
    },

    SET_OBJECT(state, object) {
        state.object = object
    }
}

